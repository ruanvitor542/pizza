var btnIncrementas = document.querySelectorAll('.btn-incrementa');


//INCREMENTANDO VALOR
for (let botao of btnIncrementas) {
    botao.addEventListener('click', incrementa)

    function incrementa(){
       var item = botao.closest('.item')

       var input = item.querySelector('.quantidade')
       input.value++

       var preco = pegaPrecoItem(item)
       addTotal(preco)
    }
}

//DECREMENTO VALOR

var btnDecrementas = document.querySelectorAll('.btn-decrementa');

for (let botao of btnDecrementas) {
    botao.addEventListener('click', decrementa)

    function decrementa(){
       var item = botao.closest('.item')

       var input = item.querySelector('.quantidade')
       
       if (input.value <= 0) {
       }else{
            input.value--
            var preco = pegaPrecoItem(item)
            addTotal(-preco)
       }
    }
}

var formsPedido = document.forms.pedido

formsPedido.addEventListener('submit', (event) => {
    var contador = 0

    var inputs = formsPedido.querySelectorAll('input.quantidade')

    for (let  input of inputs) {
        if(input.value > 0){
            contador++
        }
    }

    if (contador == 0) {
        alert('Deve ter pelo menos uma pizza no pedido')
        event.preventDefault()
    }
}
)   

//FUNCOES AUXILIARES

function pegaPrecoItem(item) {
    var precoItem = item.querySelector('.preco-item')
    return Number(precoItem.textContent)
}

function addTotal(preco) {
    var total = document.querySelector('#total')
    total.textContent = preco + Number(total.textContent)
}

